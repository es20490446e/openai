```
USAGE
       openai [option] [input]

OPTIONS
          -key
              Registers the access key provided at openai.com

          -chat
              Keeps openai open till closed

LEGAL
       This tool is under the terms of service of openai.com

       The authors of this tool aren't afiliated with openai.com
